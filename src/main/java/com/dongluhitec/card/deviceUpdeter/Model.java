package com.dongluhitec.card.deviceUpdeter;

import com.dongluhitec.card.deviceUpdeter.bean.BasicJavaBeanModel;
import com.dongluhitec.card.deviceUpdeter.bean.Device;
import com.dongluhitec.card.deviceUpdeter.bean.LabelDate;

import java.util.ArrayList;
import java.util.List;

public class Model extends BasicJavaBeanModel {
	
	private List<Device> list = new ArrayList<>();
	private String label = "正在搜索设备";
	
	public List<Device> getList() {
		return list;
	}

	public void setList(List<Device> list) {
		this.list.clear();
		this.list.addAll(list);
		pcs.firePropertyChange("list", null, this.list);
	}

	public void addList(Device device){
		this.list.add(device);
		pcs.firePropertyChange("list", null, this.list);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		pcs.firePropertyChange("label", this.label, this.label = label);
	}
	
	
}
