package com.dongluhitec.card.deviceUpdeter.bean;

public class Device extends BasicJavaBeanModel {
	
	private String mac ="";
	private boolean status;
	private String TyIp;
	private String product;
	private String labeleText="正在搜索";
	
	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		pcs.firePropertyChange("mac", this.mac, this.mac = mac);
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		pcs.firePropertyChange("status", this.status, this.status = status);
	}

	public String getTyIp() {
		return TyIp;
	}

	public void setTyIp(String tyIp) {
		pcs.firePropertyChange("tyIp", this.TyIp, this.TyIp = tyIp);
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		pcs.firePropertyChange("product", this.product, this.product = product);
	}

	public String getLabeleText() {
		return labeleText;
	}

	public void setLabeleText(String labeleText) {
		pcs.firePropertyChange("labeleText", this.labeleText, this.labeleText = labeleText);
	}
}
