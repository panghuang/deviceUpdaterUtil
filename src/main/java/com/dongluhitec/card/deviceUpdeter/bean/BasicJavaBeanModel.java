package com.dongluhitec.card.deviceUpdeter.bean;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class BasicJavaBeanModel {
	transient final protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		if (this.pcs != null)
			this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		if (this.pcs != null)
			this.pcs.removePropertyChangeListener(listener);
	}

	public PropertyChangeSupport getPropertyChangeSupport() {
		return pcs;
	}

	public void clearPropertyChangeListeners() {
		final PropertyChangeListener[] propertyChangeListeners = this.pcs.getPropertyChangeListeners();
		for (PropertyChangeListener propertyChangeListener : propertyChangeListeners) {
			this.pcs.removePropertyChangeListener(propertyChangeListener);
		}
	}
}
